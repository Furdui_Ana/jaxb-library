import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class BookToXmlTest {

    private Catalog catalog;

    @Before
    public void testXmlToObject() throws JAXBException {
        File file = new File("src\\main\\resources\\books.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        catalog = (Catalog) unmarshaller.unmarshal(file);
        System.out.println(catalog);
    }

    @Test
    public void testObjectToXml() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(catalog, new File("src\\main\\resources\\booksGenerate.xml"));
        marshaller.marshal(catalog, System.out);

    }
}
