
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "catalog")
public class Catalog {

    @XmlElement(name = "book")
    List<Book> books = new ArrayList<Book>();

    @Override
    public String toString() {
        return "catalog{" +
                ",\n book=" + books + "\n" +
                '}';
    }
}

